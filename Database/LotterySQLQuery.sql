use Lottery
CREATE TABLE Time (
	Time_Id int NOT NULL,
	Time_Name varchar(255),
	PRIMARY KEY (Time_Id)
);
CREATE TABLE DateTime (
	DT_Id int NOT NULL,
	Date varchar(255),
	Status varchar(55),
	PRIMARY KEY (DT_Id),
	Time_Id int NOT NULL,
	FOREIGN KEY (Time_Id) REFERENCES Time(Time_Id)
);
CREATE TABLE Pos (
	Pos_Id int NOT NULL,
	Pos_Name varchar(255),
	PRIMARY KEY (Pos_Id)
);
CREATE TABLE Number_Result (
	NR_Id int NOT NULL,
	Number int,
	Pos_Id int NOT NULL,
	PRIMARY KEY (NR_Id),
	FOREIGN KEY (Pos_Id) REFERENCES Pos(Pos_Id)
);
CREATE TABLE Result_Lottery (
	DT_Id int NOT NULL,
	NR_Id int NOT NULL,
	FOREIGN KEY (DT_Id) REFERENCES DateTime(DT_Id),
	FOREIGN KEY (NR_Id) REFERENCES Number_Result(NR_Id)
);
CREATE TABLE Client (
	C_Id int NOT NULL,
	C_Name varchar(255),
	PRIMARY KEY (C_Id)
);
CREATE TABLE Client_Lottery (
	CL_Id int NOT NULL,
	Number int,
	Mul int,
	Pos_Id int NOT NULL,
	PRIMARY KEY (CL_Id),
	FOREIGN KEY (Pos_Id) REFERENCES Pos(Pos_Id)
);
CREATE TABLE DateIn_Client (
	DT_Id int NOT NULL,
	C_Id int NOT NULL,
	CL_Id int NOT NULL,
	FOREIGN KEY (DT_Id) REFERENCES DateTime(DT_Id),
	FOREIGN KEY (C_Id) REFERENCES Client(C_Id),
	FOREIGN KEY (CL_Id) REFERENCES Client_Lottery(CL_Id)
);
CREATE TABLE Role (
	R_Id int NOT NULL,
	R_Name varchar(255),
	PRIMARY KEY (R_Id)
);
CREATE TABLE Users (
	U_Id int NOT NULL,
	Name varchar(255),
	Password varchar(255),
	R_Id int NOT NULL,
	FOREIGN KEY (R_Id) REFERENCES Role(R_Id)
);
